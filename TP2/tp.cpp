// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <limits>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <tgmath.h>
#include <utility> 

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/jmkdtree.h"

void MHPSS(std::vector<Vec3> &outputPositions, std::vector<Vec3> &outputNormals, std::vector<Vec3> const &positions, std::vector<Vec3> const &normals, 
          BasicANNkdTree const & kdtree, int kernel_tye, float radius, unsigned int nbIterations = 30, unsigned int knn = 20);


std::vector< Vec3 > positions;
std::vector< Vec3 > normals;

std::vector< Vec3 > positions2;
std::vector< Vec3 > normals2;

struct boundingBox{

    Vec3 max, min;
    float sizeX, sizeY, sizeZ;

    void createBoundingBox(std::vector<Vec3> positions){
		const float epsilon = 0.05;
		float Xmin = std::numeric_limits<float>::max();
		float Ymin = std::numeric_limits<float>::max();
		float Zmin = std::numeric_limits<float>::max();

		float Xmax = std::numeric_limits<float>::min();
		float Ymax = std::numeric_limits<float>::min();
		float Zmax = std::numeric_limits<float>::min();

		for(unsigned long int i = 0; i < positions.size(); i++){
			if(positions[i][0] < Xmin){Xmin=positions[i][0];}
			if(positions[i][1] < Ymin){Ymin=positions[i][1];}
			if(positions[i][2] < Zmin){Zmin=positions[i][2];}

			if(positions[i][0] > Xmax){Xmax=positions[i][0];}
			if(positions[i][1] > Ymax){Ymax=positions[i][1];}
			if(positions[i][2] > Zmax){Zmax=positions[i][2];}
		}
		Xmax += epsilon;
		Ymax += epsilon;
		Zmax += epsilon;

		Xmin -= epsilon;
		Ymin -= epsilon;
		Zmin -= epsilon;

        max = Vec3(Xmax, Ymax, Zmax);
        min = Vec3(Xmin, Ymin, Zmin);

        sizeX = Xmax - Xmin;
        sizeY = Ymax - Ymin;
        sizeZ = Zmax - Zmin;
	}
};

struct Point{
    Vec3 position;
    int id;

    int poids;
};

struct Triangle{
    Vec3 a, b, c;
};

std::vector<Triangle> triangles;
std::vector<Point> points;

struct Voxel{
    int sommet[8];
    bool aAfficher = false;
    Vec3 normale;
    std::vector<std::pair<int, int>> arreteMesh;

    Vec3 centre;

    void update(){
        for(int i = 0; i < 8; i++){
            for(int j = i+1; j < 8; j++){
                if(points[sommet[i]].poids != points[sommet[j]].poids){
                    aAfficher = true;
                    //std::pair<int, int> maPaire(sommet[i], sommet[j]);
                    //arreteMesh.push_back(maPaire);
                }
            }
        }

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(i != j){
                    std::pair<int, int> maPaire(sommet[i], sommet[j]);
                    arreteMesh.push_back(maPaire);
                }
            }
        }
    }
};


std::vector<Voxel> voxels;
std::vector<Vec3> projectedPoints;
std::vector<Vec3> normalProjected;

struct Grille{

    void createGridPoints(const boundingBox& bbox, float resolutionX, float resolutionY, float resolutionZ) {
        for (float x = bbox.min[0]; x < bbox.max[0]; x += resolutionX) {
            for (float y = bbox.min[1]; y < bbox.max[1]; y += resolutionY) {
                for (float z = bbox.min[2]; z < bbox.max[2]; z += resolutionZ) {
                    Point point;
                    point.position = Vec3(x, y, z);
                    // You can assign unique IDs and weights as needed.
                    point.id = points.size(); // Assign unique ID
                    point.poids = 0; // Assign weight as needed
                    points.push_back(point);
                }
            }
        }
    }

    void createVoxels(int gridWidth, int gridHeight, int gridDepth) {
        for (int z = 0; z < gridDepth - 1; z++) {
            for (int y = 0; y < gridHeight - 1; y++) {
                for (int x = 0; x < gridWidth - 1; x++) {
                    Voxel voxel;

                    // Define the indices of the 8 points for the current voxel
                    int baseIndex = x + y * gridWidth + z * gridWidth * gridHeight;
                    voxel.sommet[0] = baseIndex;
                    voxel.sommet[1] = baseIndex + 1;
                    voxel.sommet[2] = baseIndex + gridWidth;
                    voxel.sommet[3] = baseIndex + gridWidth + 1;
                    voxel.sommet[4] = baseIndex + gridWidth * gridHeight;
                    voxel.sommet[5] = baseIndex + gridWidth * gridHeight + 1;
                    voxel.sommet[6] = baseIndex + gridWidth * gridHeight + gridWidth;
                    voxel.sommet[7] = baseIndex + gridWidth * gridHeight + gridWidth + 1;

                    Vec3 center = Vec3(0,0,0);
                    for (int i = 0; i < 8; i++) {
                        center[0] += points[voxel.sommet[i]].position[0];
                        center[1] += points[voxel.sommet[i]].position[1];
                        center[2] += points[voxel.sommet[i]].position[2];
                    }
                    center[0] /= 8.0;
                    center[1] /= 8.0;
                    center[2] /= 8.0;

                    voxel.centre = center;

                    voxels.push_back(voxel);
                }
            }
        }
    }

    void updateWeights(const BasicANNkdTree &kdtree){
        projectedPoints.resize(points.size());
        normalProjected.resize(points.size());

        for(size_t i = 0; i < points.size(); i++){
            projectedPoints[i] = points[i].position;
        }

        MHPSS(projectedPoints, normalProjected, positions, normals, kdtree, 0, 10, 30, 20);

        for(size_t i = 0; i < points.size(); i++){
            Vec3 AtoP = Vec3(0,0,0);
            AtoP = points[i].position - projectedPoints[i];
            AtoP.normalize(); normalProjected[i].normalize();
            if(Vec3::dot(AtoP, normalProjected[i]) < 0){
                points[i].poids = -1;
            }else if(Vec3::dot(AtoP, normalProjected[i]) > 0){
                points[i].poids = 1;
            }
        }
    }

    void updateCentersToDisplay(const BasicANNkdTree &kdtree){
        int newSize = 0;
        for(size_t i = 0; i < voxels.size(); i++){
            voxels[i].update();
            if(voxels[i].aAfficher){newSize++;}
        }

        std::vector<Voxel> newVoxels;

        for(size_t i = 0; i < voxels.size(); i++){
            if(voxels[i].aAfficher){
                newVoxels.push_back(voxels[i]);
            }
        }

        std::cout<<"nombre voxels avant :"<<voxels.size()<<std::endl;
        voxels.clear();
        normalProjected.clear();
        std::cout<<"nombre voxels après clear :"<<voxels.size()<<std::endl;
        voxels.resize(newVoxels.size());
        normalProjected.resize(newVoxels.size());
        std::cout<<"nombre voxels qu'on devrait afficher :"<<newVoxels.size()<<std::endl;
        std::cout<<"nombre voxels après tout :"<<voxels.size()<<std::endl;

        for(size_t i = 0; i < newVoxels.size(); i++){
            voxels[i] = newVoxels[i];
        }

        std::vector<Vec3> positionVoxels;
        positionVoxels.resize(voxels.size());
        for(size_t i = 0; i < newVoxels.size(); i++){
            positionVoxels[i] = voxels[i].centre;
        }

        MHPSS(positionVoxels, normalProjected, positions, normals, kdtree, 0, 10, 30, 20);

        for(size_t i = 0; i < newVoxels.size(); i++){
            voxels[i].centre = positionVoxels[i];
            voxels[i].normale = normalProjected[i];
        }

        std::cout<<"test"<<std::endl;
    }

    void createTriangles(){
        int nbVoisinage;
        std::vector<Voxel> voxelsVoisins;
        for(size_t i = 0; i < voxels.size(); i++){
            for(size_t arretComptI = 0; arretComptI < voxels[i].arreteMesh.size(); arretComptI++){
                int nbVoisinArrete = 1;
                voxelsVoisins.clear();
                for(size_t j = 0; j < voxels.size(); j++){
                    for(size_t arretComptJ = 0; arretComptJ < voxels[j].arreteMesh.size(); arretComptJ++){
                        if(voxels[i].arreteMesh[arretComptI] == voxels[j].arreteMesh[arretComptJ]){
                            nbVoisinArrete++;
                            voxelsVoisins.push_back(voxels[j]);
                        }
                    }
                }
                if(nbVoisinArrete >= 4){
                    nbVoisinage++;
                    for (int i = 0; i < nbVoisinArrete; i++) {
                        for (int j = i + 1; j < nbVoisinArrete; j++) {
                            for (int k = j + 1; k < nbVoisinArrete; k++) {
                                Triangle triangle;
                                triangle.a = voxelsVoisins[i].centre;
                                triangle.b = voxelsVoisins[j].centre;
                                triangle.c = voxelsVoisins[k].centre;
                                triangles.push_back(triangle);
                            }
                        }
                    }
                }
            }
        }
        std::cout<<nbVoisinage<<std::endl;
    }
};


// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 640;
static unsigned int SCREENHEIGHT = 480;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;




// ------------------------------------------------------------------------------------------------------------
// i/o and some stuff
// ------------------------------------------------------------------------------------------------------------
void loadPN (const std::string & filename , std::vector< Vec3 > & o_positions , std::vector< Vec3 > & o_normals ) {
    unsigned int surfelSize = 6;
    FILE * in = fopen (filename.c_str (), "rb");
    if (in == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    size_t READ_BUFFER_SIZE = 1000; // for example...
    float * pn = new float[surfelSize*READ_BUFFER_SIZE];
    o_positions.clear ();
    o_normals.clear ();
    while (!feof (in)) {
        unsigned numOfPoints = fread (pn, 4, surfelSize*READ_BUFFER_SIZE, in);
        for (unsigned int i = 0; i < numOfPoints; i += surfelSize) {
            o_positions.push_back (Vec3 (pn[i], pn[i+1], pn[i+2]));
            o_normals.push_back (Vec3 (pn[i+3], pn[i+4], pn[i+5]));
        }

        if (numOfPoints < surfelSize*READ_BUFFER_SIZE) break;
    }
    fclose (in);
    delete [] pn;
}
void savePN (const std::string & filename , std::vector< Vec3 > const & o_positions , std::vector< Vec3 > const & o_normals ) {
    if ( o_positions.size() != o_normals.size() ) {
        std::cout << "The pointset you are trying to save does not contain the same number of points and normals." << std::endl;
        return;
    }
    FILE * outfile = fopen (filename.c_str (), "wb");
    if (outfile == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    for(unsigned int pIt = 0 ; pIt < o_positions.size() ; ++pIt) {
        fwrite (&(o_positions[pIt]) , sizeof(float), 3, outfile);
        fwrite (&(o_normals[pIt]) , sizeof(float), 3, outfile);
    }
    fclose (outfile);
}
void scaleAndCenter( std::vector< Vec3 > & io_positions ) {
    Vec3 bboxMin( FLT_MAX , FLT_MAX , FLT_MAX );
    Vec3 bboxMax( FLT_MIN , FLT_MIN , FLT_MIN );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        for( unsigned int coord = 0 ; coord < 3 ; ++coord ) {
            bboxMin[coord] = std::min<float>( bboxMin[coord] , io_positions[pIt][coord] );
            bboxMax[coord] = std::max<float>( bboxMax[coord] , io_positions[pIt][coord] );
        }
    }
    Vec3 bboxCenter = (bboxMin + bboxMax) / 2.f;
    float bboxLongestAxis = std::max<float>( bboxMax[0]-bboxMin[0] , std::max<float>( bboxMax[1]-bboxMin[1] , bboxMax[2]-bboxMin[2] ) );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = (io_positions[pIt] - bboxCenter) / bboxLongestAxis;
    }
}

void applyRandomRigidTransformation( std::vector< Vec3 > & io_positions , std::vector< Vec3 > & io_normals ) {
    srand(time(NULL));
    Mat3 R = Mat3::RandRotation();
    Vec3 t = Vec3::Rand(1.f);
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = R * io_positions[pIt] + t;
        io_normals[pIt] = R * io_normals[pIt];
    }
}

void subsample( std::vector< Vec3 > & i_positions , std::vector< Vec3 > & i_normals , float minimumAmount = 0.1f , float maximumAmount = 0.2f ) {
    std::vector< Vec3 > newPos , newNormals;
    std::vector< unsigned int > indices(i_positions.size());
    for( unsigned int i = 0 ; i < indices.size() ; ++i ) indices[i] = i;
    srand(time(NULL));
    std::random_shuffle(indices.begin() , indices.end());
    unsigned int newSize = indices.size() * (minimumAmount + (maximumAmount-minimumAmount)*(float)(rand()) / (float)(RAND_MAX));
    newPos.resize( newSize );
    newNormals.resize( newSize );
    for( unsigned int i = 0 ; i < newPos.size() ; ++i ) {
        newPos[i] = i_positions[ indices[i] ];
        newNormals[i] = i_normals[ indices[i] ];
    }
    i_positions = newPos;
    i_normals = newNormals;
}

bool save( const std::string & filename , std::vector< Vec3 > & vertices , std::vector< unsigned int > & triangles ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = vertices.size() , n_triangles = triangles.size()/3;
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << vertices[v][0] << " " << vertices[v][1] << " " << vertices[v][2] << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << triangles[3*f] << " " << triangles[3*f+1] << " " << triangles[3*f+2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}



// ------------------------------------------------------------------------------------------------------------
// rendering.
// ------------------------------------------------------------------------------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}

void drawTriangleMesh( std::vector< Vec3 > const & i_positions , std::vector< unsigned int > const & i_triangles ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_triangles.size() / 3 ; ++tIt) {
        Vec3 p0 = i_positions[3*tIt];
        Vec3 p1 = i_positions[3*tIt+1];
        Vec3 p2 = i_positions[3*tIt+2];
        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f( p0[0] , p0[1] , p0[2] );
        glVertex3f( p1[0] , p1[1] , p1[2] );
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }
    glEnd();
}

void renderTriangles(const std::vector<Triangle>& triangles) {
    glBegin(GL_TRIANGLES);
    //std::cout<<triangles.size()<<std::endl;
    for (const Triangle& triangle : triangles) {
        Vec3 p0 = triangle.a;
        Vec3 p1 = triangle.b;
        Vec3 p2 = triangle.c;

        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f(triangle.a[0], triangle.a[1], triangle.a[2]);
        glVertex3f(triangle.b[0], triangle.b[1], triangle.b[2]);
        glVertex3f(triangle.c[0], triangle.c[1], triangle.c[2]);
    }
    glEnd();
}

void drawPointSet( std::vector< Vec3 > const & i_positions , std::vector< Vec3 > const & i_normals ) {
    glBegin(GL_POINTS);
    for(unsigned int pIt = 0 ; pIt < i_positions.size() ; ++pIt) {
        glNormal3f( i_normals[pIt][0] , i_normals[pIt][1] , i_normals[pIt][2] );
        glVertex3f( i_positions[pIt][0] , i_positions[pIt][1] , i_positions[pIt][2] );
    }
    glEnd();
}

void drawCenter(){
    glColor3f(1.0f, 0.0f, 0.0f);
    glPointSize(2.0f);
    glBegin(GL_POINTS);
    for(unsigned int i = 0 ; i < voxels.size() ; ++i) {
        glVertex3f(voxels[i].centre[0] , voxels[i].centre[1] , voxels[i].centre[2]);
    }
    glEnd();
}

void drawPositions(){
    glBegin(GL_POINTS);
    for(size_t i = 0; i < points.size(); i++){
        glColor3f(8.0f, 8.0f, 1.0f);
        glPointSize(2.0f);
        if(points[i].poids > 0){
            glColor3f(0.0f, 8.0f, 1.0f);
            glPointSize(2.0f);
        }
        glVertex3f(points[i].position[0] , points[i].position[1] , points[i].position[2]);
    }
    glEnd();
}

void draw () {
    glPointSize(2); // for example...

    glColor3f(0.8,0.8,1);
    //drawPointSet(positions , normals);
    //drawCenter();
    renderTriangles(triangles);
    //drawPositions();
    //glColor3f(0.8,0.8,0.1);
    //drawPointSet(positions2 , normals2);
}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;

    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break; 

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}

void bruitMesh(std::vector<Vec3>& positions, std::vector<Vec3>& normals, float noiseLevel) {
    for (size_t i = 0; i < normals.size(); ++i) {
        
        float noiseX = static_cast<float>(std::rand()) / RAND_MAX * noiseLevel;
        float noiseY = static_cast<float>(std::rand()) / RAND_MAX * noiseLevel;
        float noiseZ = static_cast<float>(std::rand()) / RAND_MAX * noiseLevel;

        normals[i][0] += noiseX;
        normals[i][1] += noiseY;
        normals[i][2] += noiseZ;

        normals[i].normalize();
    }
}

struct Plan{
    Vec3 point;
    Vec3 normal;
};

Plan calculPlan(std::vector<Vec3> positions, std::vector<Vec3> normales){
    Plan plan;
    plan.point = Vec3(0,0,0);
    plan.normal = Vec3(0,0,0);

    Vec3 sommePos = Vec3(0,0,0);
    Vec3 sommeNorm = Vec3(0,0,0);

    unsigned int nbPoints = positions.size();

    for(unsigned int i = 0; i < nbPoints; i++){
        sommePos += positions[i];
        sommeNorm += normales[i];  
    }

    plan.point = Vec3(sommePos[0]/nbPoints, sommePos[1]/nbPoints, sommePos[2]/nbPoints);
    plan.normal = Vec3(sommeNorm[0]/nbPoints, sommeNorm[1]/nbPoints, sommeNorm[2]/nbPoints);

    plan.normal.normalize();

    return plan;
}

Vec3 projectOnPlan(Plan &plan, Vec3 pointToProject){
    return pointToProject - ((Vec3::dot(pointToProject - plan.point, plan.normal)/(plan.normal.length()) * plan.normal));
}

void SPSS(std::vector<Vec3> const &inputPositions, std::vector<Vec3> const &inputNormals, std::vector<Vec3> &outputPositions, std::vector<Vec3> &outputNormals, unsigned int knn, BasicANNkdTree const & kdtree){
    for(unsigned int i = 0; i < outputPositions.size(); i++){
        Plan plan;
        plan.point = Vec3(0,0,0);
        plan.normal = Vec3(0,0,0);

        ANNidxArray idArrayNearests = new ANNidx[knn];
        ANNdistArray distNeighbors = new ANNdist[knn];

        kdtree.knearest(outputPositions[i], knn, idArrayNearests, distNeighbors);

        std::vector<Vec3> posNeighbors;
        std::vector<Vec3> normNeighbors;

        for(unsigned int j = 0; j < knn; j++){
            posNeighbors.push_back(inputPositions[idArrayNearests[j]]);
            normNeighbors.push_back(inputNormals[idArrayNearests[j]]);
        }

        plan = calculPlan(posNeighbors, normNeighbors);

        outputPositions[i] = projectOnPlan(plan, outputPositions[i]);
        outputNormals[i] = plan.normal;
    }
}

void MPSS(std::vector<Vec3> &outputPositions, std::vector<Vec3> &outputNormals, std::vector<Vec3> const &positions, std::vector<Vec3> const &normals, 
          BasicANNkdTree const & kdtree, unsigned int nbIterations = 30, unsigned int knn = 20){

    for(unsigned int a = 0; a < nbIterations; a++){
        SPSS(positions, normals, outputPositions, outputNormals, knn, kdtree);
    }
}

void HPSS(std::vector<Vec3> &outputPositions, std::vector<Vec3> &outputNormals, std::vector<Vec3> const &positions, std::vector<Vec3> const &normals, 
          BasicANNkdTree const & kdtree, int kernel_tye, float radius, unsigned int knn){
    
    for(unsigned int i = 0; i < outputPositions.size(); i++){
        Plan planVoisin;

        ANNidxArray idArrayNearests = new ANNidx[knn];
        ANNdistArray distNeighbors = new ANNdist[knn];

        kdtree.knearest(outputPositions[i], knn, idArrayNearests, distNeighbors);

        float poidsTotal = 0;
        Vec3 sumPos = Vec3(0,0,0);
        Vec3 sumNorm = Vec3(0,0,0);

        for(unsigned int j = 0; j < knn; j++){
            planVoisin.point = Vec3(0,0,0);
            planVoisin.normal = Vec3(0,0,0);
            planVoisin.point = positions[idArrayNearests[j]];
            planVoisin.normal = normals[idArrayNearests[j]];

            Vec3 posProjected = Vec3(0,0,0);
            Vec3 normProjected = Vec3(0,0,0);

            posProjected = projectOnPlan(planVoisin, outputPositions[i]);
            normProjected = planVoisin.normal;

            float poid = 0;
            float distance = (outputPositions[i] - posProjected).length(); 

            poid = exp(-pow(distance,2)/pow(radius,2));
            
            sumPos += poid * posProjected;
            sumNorm += poid * normProjected;

            poidsTotal += poid; 
        }
        
        outputPositions[i] = sumPos/poidsTotal;
        outputNormals[i] = sumNorm/poidsTotal;     
    }
}

void MHPSS(std::vector<Vec3> &outputPositions, std::vector<Vec3> &outputNormals, std::vector<Vec3> const &positions, std::vector<Vec3> const &normals, 
          BasicANNkdTree const & kdtree, int kernel_tye, float radius, unsigned int nbIterations, unsigned int knn){

    for(unsigned int a = 0; a < nbIterations; a++){
        HPSS(outputPositions, outputNormals, positions, normals, kdtree, kernel_tye, radius, knn);
    }
}


int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("tp dual contouring");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);


    // Load a first pointset, and build a kd-tree:
    loadPN("pointsets/igea.pn" , positions , normals);
    BasicANNkdTree kdtree;
    kdtree.build(positions);

    boundingBox bbox;
    bbox.createBoundingBox(positions);
    Grille grille;
    grille.createGridPoints(bbox, bbox.sizeX/64.0, bbox.sizeY/64.0, bbox.sizeZ/64.0);
    grille.createVoxels(64,64,64);
    grille.updateWeights(kdtree);
    grille.updateCentersToDisplay(kdtree);
    grille.createTriangles();



    glutMainLoop ();
    return EXIT_SUCCESS;
}
